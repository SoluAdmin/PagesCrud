<?php

namespace SoluAdmin\PagesCrud\Models;

use Backpack\CRUD\CrudTrait;
use Backpack\CRUD\ModelTraits\SpatieTranslatable\HasTranslations;
use Illuminate\Database\Eloquent\Model;
use SoluAdmin\Support\Traits\SafeSluggable;
use SoluAdmin\Support\Traits\SafeSluggableScopeHelpers;

class Page extends Model
{
    use CrudTrait;
    use SafeSluggable;
    use SafeSluggableScopeHelpers;
    use HasTranslations;

    protected $fillable = ['template', 'name', 'title', 'slug', 'extras', 'content'];
    protected $fakeColumns = ['extras'];
    protected $translatable = ['title', 'slug', 'extras', 'content'];
    protected $casts = ['extras' => 'array'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'slug_or_title',
            ],
        ];
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getTemplateName()
    {
        return trans('page-templates.' . $this->template);
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getSlugOrTitleAttribute()
    {
        return ($this->slug != '') ? $this->slug : $this->title;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
