<?php

namespace SoluAdmin\PagesCrud\PageTemplates;

use App\Models\Permission\User;
use Backpack\CRUD\CrudPanel;

class PageTemplateParser
{
    private $crud;

    public function __construct(CrudPanel $crud)
    {
        $this->crud = $crud;
    }

    public function getTemplatesOptions()
    {
        $templatesOptions = [];
        $templates = $this->getTemplates();

        foreach ($templates as $template) {
            $templatesOptions[$template->name] = \Lang::has('page-templates.' . $template->name)
                ? trans('page-templates.' . $template->name)
                : $this->crud->makeLabel($template->name);
        }

        return $templatesOptions;
    }

    public function getTemplateFields($template)
    {
        $templateClass = $this->getTemplatesClass();
        $templates = $this->getTemplates();

        $templateName = ($template == false) ? array_first($templates)->name : $template;

        return (new $templateClass)->{$templateName}();
    }

    protected function getTemplatesClass()
    {
        $namespace = config('SoluAdmin.PagesCrud.templates_namespace');

        /** @var User $user */
        $user = auth()->user();
        $tenant = $user->defaultTenantByType('website');
        $userTemplates = $namespace . studly_case($tenant->name);
        $templatesClassName = class_exists($userTemplates) ? $userTemplates : BasePageTemplates::class;

        return $templatesClassName;
    }

    private function getTemplates()
    {
        $templatesClassName = $this->getTemplatesClass();
        $reflector = new \ReflectionClass($templatesClassName);
        $templates = $reflector->getMethods(\ReflectionMethod::IS_PUBLIC);

        $templates = array_filter($templates, function ($method) {
            return $method->name !== '__construct';
        });

        if (!count($templates)) {
            abort('403', 'No templates have been found.');
        }

        return $templates;
    }
}
