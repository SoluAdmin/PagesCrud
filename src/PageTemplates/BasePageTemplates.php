<?php


namespace SoluAdmin\PagesCrud\PageTemplates;

use Illuminate\Support\Collection;

class BasePageTemplates
{
    protected $fields;

    public function __construct()
    {
        $this->fields = new Collection();
    }

    public function basic()
    {
        $this->addMetas();
        $this->addContent();
        return $this->getFields();
    }

    protected function getFields()
    {
        return $this->fields->toArray();
    }

    protected function addMetas()
    {
        $this->fields->push([
            'name' => 'metas_separator',
            'type' => 'custom_html',
            'value' => '<br><h2>' . trans('SoluAdmin::PagesCrud.fields.meta.label') . '</h2><hr>',
        ]);

        $this->fields->push([
            'name' => 'meta_description',
            'label' => trans('SoluAdmin::PagesCrud.fields.meta.description'),
            'fake' => true,
            'store_in' => 'extras',
            'fake_translated' => true,
        ]);

        $this->fields->push([
            'name' => 'meta_keywords',
            'label' => trans('SoluAdmin::PagesCrud.fields.meta.keywords'),
            'fake' => true,
            'store_in' => 'extras',
            'fake_translated' => true,
        ]);
    }

    protected function addContent()
    {
        $this->fields->push([
            'name' => 'content_separator',
            'type' => 'custom_html',
            'value' => '<br><h2>' . trans('SoluAdmin::PagesCrud.fields.content') . '</h2><hr>',
        ]);

        $this->fields->push([
            'name' => 'content',
            'label' => trans('SoluAdmin::PagesCrud.fields.content'),
            'type' => 'ckeditor',
        ]);
    }
}
