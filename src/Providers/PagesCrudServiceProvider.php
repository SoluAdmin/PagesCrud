<?php

namespace SoluAdmin\PagesCrud\Providers;

use Backpack\CRUD\CrudPanel;
use Illuminate\Routing\Router;
use SoluAdmin\PagesCrud\PageTemplates\PageTemplateParser;
use SoluAdmin\Support\Providers\CrudServiceProvider;
use SoluAdmin\Support\Helpers\PublishableAssets as Assets;

class PagesCrudServiceProvider extends CrudServiceProvider
{
    protected $assets = [
        Assets::CONFIGS,
        Assets::TRANSLATIONS,
        Assets::MIGRATIONS,
        Assets::MODULE_MIGRATIONS,
        Assets::VIEWS,
    ];

    protected $resources = [
        'page' => "PageCrudController"
    ];

    protected function bootExtras()
    {
        $this->publishes([__DIR__. '/../../resources/extras/views' => base_path('resources/views')], 'views');
    }

    protected function registerExtras()
    {
        $this->app->router->group(['namespace' => 'SoluAdmin\PagesCrud\Http\Controllers'], function (Router $router) {
            $router->group([
                'middleware' => config('SoluAdmin.PagesCrud.middleware')
                    ? array_merge(['web', 'admin'], config('SoluAdmin.PagesCrud.middleware'))
                    : ['web', 'admin'],
                'prefix' => config('backpack.base.route_prefix', 'admin')
            ], function (Router $router) {
                $router->get('page/create/{template}', 'PageCrudController@create');
                $router->get('page/{id}/edit/{template}', 'PageCrudController@edit');
            });
        });

        $this->app->bind(PageTemplateParser::class, function () {
            return new PageTemplateParser(app()->make(CrudPanel::class));
        });
    }
}
