<?php

namespace SoluAdmin\PagesCrud\Http\Forms;

use SoluAdmin\PagesCrud\PageTemplates\PageTemplateParser;
use SoluAdmin\Support\Interfaces\Form;

class PageCrudForm implements Form
{
    private $parser;

    public function __construct()
    {
        $this->parser = app()->make(PageTemplateParser::class);
    }

    public function fields()
    {
        $template = \Route::current()->parameter('template');

        return [
            [
                'name' => 'template',
                'label' => trans('SoluAdmin::PagesCrud.template'),
                'type' => 'select_page_template',
                'options' => $this->parser->getTemplatesOptions(),
                'value' => $template,
                'allows_null' => false,
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-6',
                ],
            ],

            [
                'name' => 'name',
                'label' => trans('SoluAdmin::PagesCrud.name'),
                'type' => 'text',
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-6',
                ],
            ],

            [
                'name' => 'title',
                'label' => trans('SoluAdmin::PagesCrud.title'),
                'type' => 'text',
            ],

            [
                'name' => 'slug',
                'label' => trans('SoluAdmin::PagesCrud.url'),
                'type' => 'text',
                'hint' => trans('SoluAdmin::PagesCrud.url_hint'),
            ],
        ];
    }

    public function templateFields($template)
    {
        return $this->parser->getTemplateFields($template);
    }
}
