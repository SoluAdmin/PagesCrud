<?php

namespace SoluAdmin\PagesCrud\Http\DataTables;

use SoluAdmin\Support\Interfaces\DataTable;

class PageCrudDataTable implements DataTable
{
    public function columns()
    {

        return [
            [
                'name' => 'name',
                'label' => trans('SoluAdmin::PagesCrud.name'),
            ],
            [
                'name' => 'slug',
                'label' => trans('SoluAdmin::PagesCrud.url'),
            ],

            [
                'name' => 'template',
                'label' => trans('SoluAdmin::PagesCrud.template'),
                'type' => 'model_function',
                'function_name' => 'getTemplateName',
            ],
        ];
    }
}
