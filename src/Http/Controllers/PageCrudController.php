<?php

namespace SoluAdmin\PagesCrud\Http\Controllers;

use SoluAdmin\PagesCrud\Http\Requests\PageCrudRequest;
use SoluAdmin\Support\Http\Controllers\BaseCrudController;

class PageCrudController extends BaseCrudController
{
    public function create($template = false)
    {
        $this->useTemplate($template);
        return parent::create();
    }

    public function store(PageCrudRequest $request)
    {
        $this->prepareRequestForSaving($request);
        return parent::storeCrud($request);
    }

    public function edit($id, $template = false)
    {
        $template = ($template == false) ? $this->figureTemplateUsed($id) : $template;
        $this->useTemplate($template);
        return parent::edit($id);
    }

    public function update(PageCrudRequest $request)
    {
        $this->prepareRequestForSaving($request);
        return parent::updateCrud($request);
    }

    private function useTemplate($template = false)
    {
        $this->crud->addFields($this->form()->templateFields($template));
    }

    private function figureTemplateUsed($id)
    {
        $model = $this->crud->model;
        $this->data['entry'] = $model::findOrFail($id);
        $template = $this->data['entry']->template;

        return $template;
    }

    private function prepareRequestForSaving(PageCrudRequest $request)
    {
        $template = $request->get('template');
        $this->useTemplate($template);
    }
}
