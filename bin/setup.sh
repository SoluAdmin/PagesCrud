#!/bin/sh

cp bin/pre-commit .git/hooks/pre-commit
chmod +x .git/hooks/pre-commit

cp bin/pre-push .git/hooks/pre-push
chmod +x .git/hooks/pre-push