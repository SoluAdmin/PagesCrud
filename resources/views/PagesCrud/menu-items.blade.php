@module("SoluAdmin\\PagesCrud")
<li>
    <a href="{{ url(config('backpack.base.route_prefix', 'admin') . config('SoluAdmin.PagesCrud.route_prefix', '') . '/page') }}">
        <i class="fa fa-edit"></i> <span>{{trans('SoluAdmin::PagesCrud.page_plural')}}</span>
    </a>
</li>
@endmodule