<?php

return [
    'route_prefix' => '',
    'middleware' => false,
    'setup_routes' => false,
    'publishes_migrations' => false,
    'templates_namespace' => "App\\PageTemplates\\",
];
